#include <stdlib.h>
#include <stdio.h>

int ft_pow(int num, int pow)
{
	if (pow == 0)
		return (1);
	else
		return (num * ft_pow(num, pow - 1));
}

char *ft_itoa_base(int value, int base)
{
	int neg;
	char *nbr;
	int cnt;

	neg = 0;
	cnt = 1;
	if (value < 0)
	{
		if (base == 10)
			neg = 1;
		value *= -1;
	}
	while(ft_pow(base, cnt) - 1 < value)
		cnt++;
	nbr = (char *)malloc(sizeof(char) * cnt);
	nbr[cnt + neg] = '\0';
	while (cnt-- > 0)
	{
		nbr[cnt + neg] = (value % base) + (value % base > 9 ? 'A' - 10 : '0');
		value /= base;
	}
	if (neg == 1)
		nbr[0] = '-';
	return (nbr);
}
/*
int ft_pow(int nb, int pow)
{
	if (pow == 0)
		return (1);
	else
		return (nb * ft_pow(nb, pow - 1));
}

char *ft_itoa_base(int value, int base)
{
	int i;
	char*nbr;
	int neg;

	i = 1;
	neg = 0;
	if (value < 0)
	{
		if (base == 10)
			neg = 1;
		value *= -1;
	}
	while (ft_pow(base, i) - 1 < value)
		i++;
	nbr = (char*)malloc(sizeof(nbr) * i);
	nbr[i + neg] = '\0';
	while (i-- > 0)
	{
		nbr[i + neg] = (value % base) + (value % base > 9 ? 'A' - 10 : '0');
		value = value / base;
	}
	if (neg)
		nbr[0] = '-';
	return (nbr);
}
*/
int main(int argc, char **argv)
{
	printf("%s", ft_itoa_base(atoi(argv[1]), atoi(argv[2])));
	return(0);
}
