#include <unistd.h>

int ft_strchr(char *str, char chr)
{
	int cnt;

	cnt = 0;
	while (str[cnt])
	{
		if (str[cnt] == chr)
			return (cnt);
		cnt++;
	}
	return (-1);
}

int ft_brackets(char *expr, int cnt, int ind)
{
	char open[] = "{[(";
	char closed[] = "}])";
	int num, tmp;

	while (expr[cnt])
	{
		if ((num = ft_strchr(closed, expr[cnt])) >= 0)
		{
			if (num == ind)
				return (cnt);
			else
				return (-1);
		}
		else if((num = ft_strchr(open, expr[cnt])) >= 0)
		{
			if ((tmp = ft_brackets(expr, cnt + 1 , num)) != -1)
				cnt = tmp;
			else
			   	return (-1);
		}
		cnt++;
	}
	if (ind != -1)
		return (-1);
	return (0);
}

int main(int argc, char *argv[])
{
	int cnt;
	int ret;

	ret = 0;
	cnt = 1;
	while (cnt < argc)
	{
		if (!ft_brackets(argv[cnt], 0, -1))
			write(1, "OK", 2);
		else
			write(1, "Error", 5);
		cnt++;	
		write(1, "\n", 1);
	}
	if (cnt == 0) write(1, "\n", 1);
	return(ret);
}
